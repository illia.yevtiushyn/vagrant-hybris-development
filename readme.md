# Vagrant Hybris Development Environment

This is a hybris development environment wrapped in vagrantfile.

## Installation guide

1. Install [virtualbox](https://www.virtualbox.org/)
2. Install [vagrant](https://www.vagrantup.com/)
3. Install vagrant [disksize plugin](https://github.com/sprotheroe/vagrant-disksize)
4. Pull this repo
5. Place archive with hybris commerce suite near the vagrantfile
6. Open vagrantfile and customize options according to your preferences
7. Open console and run: `vagrant up`

Once you see words "System booted and ready" - feel free to connect via ssh and start hacking!

## List of contents

- OpenJDK 1.8
- MySQL Server
- ImageMagick
- Git SCM
- NFS Server

## Connecting to the NFS share

By default whole /opt/ directory is shared through the NFS server. So, for example, in order to connect to hybris folder from the windows host you need to enable nfs services and run next command:

```bat
 mount -o fileaccess=777 -o anon \\machine-ip-address\opt\hybris Y:
```

## Some useful aliases

| Alias | Description |
| ------ | ------ |
| `cdh` | Navigate to hybris platform dir |
| `cdc` | Navigate to hybris config dir |
| `cdl` | Navigate to hybris logs dir |
| `sae` | Call setantenv |
| `acu` | Call ant customize |
| `aca` | Call ant clean all |
| `aal` | Call ant all |
| `abu` | Call ant build |
| `ain` | Call ant initialize |
| `aup` | Call ant updatesystem |
| `sca` | Start hybris server |
| `sde` | Debug hybris server |
